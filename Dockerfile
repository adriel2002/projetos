# Oficial image for nginx alpine
FROM nginx:1.16.1
LABEL maintainer="rhlpolitano@gmail.com"

# Working directory
WORKDIR /usr/src/adrielsite

# Copy essential files from your host the project
COPY site/ projetos

# Change owner of directory /usr/src/adrielsite to nginx user and group
RUN chown -R nginx:nginx /usr/src/adrielsite

# Copy your nginx configuration file from your host to the project
COPY nginxconf/default.conf /etc/nginx/conf.d/default.conf

# Expose port 80 to host
EXPOSE 80

# Run nginx
RUN nginx -t

# Makes nginx run in foreground, not background, so it stays running
CMD nginx -g 'daemon off;'