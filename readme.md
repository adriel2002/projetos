# Manual on how to deploy the application.

Hello! You will learn how to **deploy** an application using **docker**. 

This "application" will be your guide.

## How to install Docker, Git on Linux and clone a repository?

To start you will to open your terminal as a admin/root user.


Run in terminal:
>apt-get update -y

>apt-get install docker -y 

- That will update Linux dependencies and install docker auto-accepting pop-ups.


Run in terminal:
>apt-get install git -y

- That will install git auto-accepting pop-ups, this is reponsible for downloading the project.


Run in terminal:
>docker --version

- This will check docker version and if you are running it.


Run in terminal:
>git --version

- This will check if git is running and what version.

Now you will need to make a folder to store your cloned project, select a main folder and do the command bellow:

Run in terminal:
>mkdir cloneprojects
>cd cloneprojects

- This will make a folder named cloneprojects, so you can store it separated from other projects.

If everything is okay, then you can click on "Clone" button on the right of the project site at GitHub or GitLab. Select "Clone with HTTPS" and copy the link bellow the button.

Run in terminal:
>git clone https://gitlab.com/adriel2002/projetos.git

- This will clone the repository to your local machine folder, created some steps ago.

## How to deploy the application on Docker?

First you will need to build the image, go to the folder containing the Dockerfile and do the following.

Run in terminal:
>docker build . -t nameofimage:1.0.0

- This will build the image and download everything necessary to run the site application via http using nginx.

Run in terminal:
>docker run -p 8090:80 nameofimage:1.0.0

- This will run the container with the image that you just built and expose the 80 port of container to the 8090 port of localhost.

Open your browser and type:
>http://localhost:8090

This will acess the newly created site running on nginx.

#DocumentVersion:2.0.0